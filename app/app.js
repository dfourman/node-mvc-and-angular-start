var express = require('express');
var logger = require('morgan');
var session = require('express-session');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var engine = require('ejs-mate');
var passport = require('passport-local');
var tempdata = require('tempdata');
var cors = require('cors');
var app = module.exports = express();

//Using EJS here
app.engine('ejs',engine);
app.set('views',__dirname+'/views');
app.set('view engine','ejs');
app.response.message = function(msg){
    var sess = this.req.session;
    sess.messages = sess.messages || [];
    sess.messages.push(msg);
    return this;
};


//Uncomment to use passport authentication
// passport.use(new passportStrat(
//     function(username,password,done){
//         return done(null,user);
//     }
// ));
//
// passport.serializeUser(function (user,cb){
//     cb(null,user);
// });
//
// passport.deserializeUser(function(id,cb){
//     cb(null,id);
// })

//Logger Info
if(!module.parent){
    app.use(logger('dev'));
}

//Opening up several folders, for our JS/CSS content, and some scripts needed by angular
app.use(express.static(__dirname + '/content'));
app.use('/node_scripts',express.static(__dirname+"/../node_modules/"));
app.use('/root_scripts',express.static(__dirname+"/../"));
app.use('/components',express.static(__dirname+"/components"));

//Session Info
app.use(session({
    resave:false,
    saveUninitialized:false,
    secret:'some secret here'
}));

//Uncomment below to use passport auth
// app.use(passport.initialize());
// app.use(passport.session());

//Tempdata use similar to that offered by ASP.NET
app.use(tempdata);

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(methodOverride('_method'));


//Internal messenger used for page routing
app.use(function(req,res,next){
    var msgs = req.session.messages||[];
    res.locals.messages = msgs;
    res.locals.hasMessagfes == !! msgs.length;
    next();
    req.session.messages = [];
});

//Controller Load
require('./appstart/routes')(app, {verbose:!module.parent});

//Page for 500 errors
app.use(function(err,req,res,next){
   if(!module.parent){
       console.error(err.stack);
   }
   res.status(500).render('5xx');
});


//Page for 404 errors
app.use(function(req,res,next){
   res.status(404).render('404',{url: req.originalUrl});
});

//Uncomment below for CORS enable - common in intranet apps
// app.use(cors());


if(!module.parent){
    app.listen(3000);
    console.log('Express started on port 3000');
}
